select {i.uid}
from {ContactUsItemComponent as component
join CMSParagraphComponent as i on {component.information}={i.pk}
join CMSContactUsItemsRel as r on {r.target}={component.pk}
join ContactUsComponent as p on {r.source}={p.pk}
join RestrictionsForComponents as rcr on {rcr.source}={component.pk} join CMSShippingCountryRestriction as r on {rcr.target}={r.pk} join CMSShippingCountryRestrictionRel as crr on {crr.source}={r.pk} join Country as c on {crr.target}={c.pk}
join CatalogVersion as v on {component.catalogVersion}={v.pk}}
where {v.version}='Online'
and {p.uid} = 'MyStylistContactUsComponent'
and {c.isocode}='JPN'


