#!/bin/bash
. messagesFiles.sh

mkdir -p ./$exportToFolder

for messageFile in "${messageFiles[@]}"
do
    fileName=${messageFile%%:*}
    folderPath=${messageFile#*:}

    cp ../../../../../$folderPath/base.properties ./$exportToFolder/$fileName.properties
done
