#!/bin/bash

exportToFolder=baseProperties

declare -a messageFiles=(
                "base:mfstorefront/web/webroot/WEB-INF/messages"
                "mffacades_base:mffacades/resources/messages"
                "mfdhl_base:mfdhl/acceleratoraddon/web/webroot/WEB-INF/messages"
                "mfloyaltyaddon_base:mfloyaltyaddon/acceleratoraddon/web/webroot/WEB-INF/messages"
                )
