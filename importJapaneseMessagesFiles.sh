#!/bin/bash
. messagesFiles.sh

for messageFile in "${messageFiles[@]}"
do
    fileName=${messageFile%%:*}
    folderPath=${messageFile#*:}

    cp ./$exportToFolder/$fileName.properties ../../../../../$folderPath/${fileName}_ja.properties
done
